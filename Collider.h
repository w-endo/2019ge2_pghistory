#pragma once
#include "Global.h"

class Collider
{
	D3DXVECTOR3		center_;
	float			radius_;
	IGameObject*	owner_;


public:
	Collider(IGameObject* owner, D3DXVECTOR3 center, float radius);
	~Collider();

	bool IsHit(Collider& target);
};

