﻿#include "SceneManager.h"
#include "PlayScene.h"
#include "TestScene.h"

SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_PLAY;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_PLAY;
IGameObject* SceneManager::pCurrentScene_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{

}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	pCurrentScene_ = CreateGameObject<PlayScene>(this);
}

//更新
void SceneManager::Update() 
{
	if (currentSceneID_ != nextSceneID_)
	{
		auto scene = childList_.begin();
		(*scene)->ReleaseSub();
		SAFE_DELETE(*scene);
		childList_.clear();

		switch (nextSceneID_)
		{
		case SCENE_ID_PLAY:
			pCurrentScene_ = CreateGameObject<PlayScene>(this);
			break;

		case SCENE_ID_TEST:
			pCurrentScene_ = CreateGameObject<TestScene>(this);
			break;
		}
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
