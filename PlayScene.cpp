﻿#include "PlayScene.h"
#include "Player.h"
#include "Enemy.h"
#include "Camera.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"),
	pCamera_(nullptr), camPosY_(0), camTarY_(0)
{
}

//初期化
void PlayScene::Initialize()
{
	Player *pOden = CreateGameObject<Player>(this);

	pCamera_ = CreateGameObject<Camera>(this);
	pCamera_->SetPosition(D3DXVECTOR3(0, 5, -10));

	CreateGameObject<Enemy>(this);
}

//更新
void PlayScene::Update()
{
	//if (Input::IsKey(DIK_UP))	camPosY_ += 0.1f;
	//if (Input::IsKey(DIK_DOWN))	camPosY_ -= 0.1f;
	//if (Input::IsKey(DIK_W))	camTarY_ += 0.1f;
	//if (Input::IsKey(DIK_S))	camTarY_ -= 0.1f;
	//   	 
	//pCamera_->SetPosition(D3DXVECTOR3(0, camPosY_, -10));
	//pCamera_->SetTarget(D3DXVECTOR3(0, camTarY_, 0));



	if (Input::IsKeyDown(DIK_SPACE))
	{
		CreateGameObject<Player>(this);
	}
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}