﻿#pragma once

#include "global.h"

class Camera;

//■■シーンを管理するクラス
class PlayScene : public IGameObject
{
	Camera* pCamera_;
	float camPosY_, camTarY_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};