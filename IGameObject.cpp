﻿#include "IGameObject.h"
#include "Global.h"
#include "Collider.h"

IGameObject::IGameObject() :
	IGameObject(nullptr, "")
{
}

IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")
{
}

IGameObject::IGameObject(IGameObject * parent, const std::string & name):
	pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)),
	dead_(false),pCollider_(nullptr)
{
}


IGameObject::~IGameObject()
{
	SAFE_DELETE(pCollider_);
}

void IGameObject::Transform()
{
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;

	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);

	localMatrix_ = matS * matRY * matRX * matRZ * matT;

	if (pParent_ != nullptr)
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	else
		worldMatrix_ = localMatrix_;

}

void IGameObject::UpdateSub()
{
	Update();
	Transform();

	if (pCollider_ != nullptr)
	{
		Collision(SceneManager::GetCurrentScene());
	}
	


	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = childList_.erase(it);
		}
		else
		{
			it++;
		}
	}

}

void IGameObject::DrawSub()
{

	Draw();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

void IGameObject::KillMe()
{
	dead_ = true;
}

void IGameObject::SetCollider(D3DXVECTOR3 & center, float radius)
{
	pCollider_ = new Collider(this, center, radius);
}

void IGameObject::Collision(IGameObject* targetObject)
{
	if (targetObject != this &&
		targetObject->pCollider_ != nullptr &&
		pCollider_->IsHit(*targetObject->pCollider_))
	{
		//当たった
		OnCollision(targetObject);
	}

	for (auto itr = targetObject->childList_.begin();
		itr != targetObject->childList_.end(); itr++)
	{
		Collision(*itr);
	}
}

void IGameObject::OnCollision(IGameObject * target)
{
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	childList_.push_back(pObj);
}